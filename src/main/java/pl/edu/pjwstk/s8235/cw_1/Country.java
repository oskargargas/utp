package pl.edu.pjwstk.s8235.cw_1;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 30.09.2011
 * Time: 08:52
 */
public class Country {
    private String name, capital;
    private double population;
    private Icon flag;

    public Country(String name, String capital, double population, Icon flag) {
        super();
        this.name = name;
        this.capital = capital;
        this.population = population;
        this.flag = flag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public void setPopulation(double population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public double getPopulation() {
        return population;
    }

    public Icon getFlag() {
        return flag;
    }

//    public void setFlag(Icon flag) {
//        this.flag = flag;
//    }

    @Override
    public String toString() {
        return name + " " + capital + " " + population;
    }
}
