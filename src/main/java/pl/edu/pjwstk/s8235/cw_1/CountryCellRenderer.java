package pl.edu.pjwstk.s8235.cw_1;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 04.10.2011
 * Time: 12:33
 */
public class CountryCellRenderer implements TableCellRenderer {

    public Component getTableCellRendererComponent(JTable jTable, Object o, boolean isSelected, boolean hasFocus, int r, int c) {

        JLabel l = new JLabel();
        double people = (Double)jTable.getModel().getValueAt(r, 2);

        if (people > 20000000)
            l.setForeground(Color.red);

        l.setText("" + o);

        return l;
    }

}
