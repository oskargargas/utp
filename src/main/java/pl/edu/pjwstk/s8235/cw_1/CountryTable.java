package pl.edu.pjwstk.s8235.cw_1;

import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import javax.swing.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 30.09.2011
 * Time: 08:44
 */
public class CountryTable extends JFrame {

    public CountryTable() {
        String[] headers = {"Name", "Capital", "Population", "Flag"};

        String json = urlToString("http://ws.geonames.org/countryInfoJSON");
        Map parsedJSON = (Map)JSONValue.parse(json);
        List<Country> countries = parseCountries((JSONArray)parsedJSON.get("geonames"));

        CountryTableModel cta = new CountryTableModel(countries, headers);

        JTable table = new JTable(cta);
        table.getColumnModel().getColumn(0).setCellRenderer(new CountryCellRenderer());

        add(new JScrollPane((table)));

        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);

    }

    private String urlToString(String url) {
        URLConnection con;
        StringBuilder sb = new StringBuilder();
        try {
            con = new URL(url).openConnection();
            InputStream in = con.getInputStream();

            BufferedReader bf = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = bf.readLine()) != null)
                sb.append(line);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private List<Country> parseCountries(JSONArray countries) {

        int countriesCount = countries.size();
        int counter = 0;

        List<Country> retCountries = new ArrayList<Country>();

        for(Map c : (List<Map<String, Object>>)countries) {
           ImageIcon icon = null;
           try {
               URL flagURL;
               flagURL = new URL("http://www.geonames.org/flags/x/" + c.get("countryCode").toString().toLowerCase() + ".gif");
               icon = new ImageIcon(flagURL);
           } catch (MalformedURLException e) {
               e.printStackTrace();
           }

           Country country = new Country((String)c.get("countryName"), (String)c.get("capital"), Double.parseDouble((String)c.get("population")), icon);
           retCountries.add(country);
           System.out.println("Parsed: " + ++counter + "/" + countriesCount);
        }

        return retCountries;
    }

    public static void main(String[] args) {
        new CountryTable();
    }
}
