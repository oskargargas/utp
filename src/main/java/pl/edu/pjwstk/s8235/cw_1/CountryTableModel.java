package pl.edu.pjwstk.s8235.cw_1;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 30.09.2011
 * Time: 08:59
 */
public class CountryTableModel extends AbstractTableModel {

    private List<Country> rows;
    private String[] colNames;

    public CountryTableModel(List<Country> lc, String[] colNames) {
        rows = lc;
        this.colNames = colNames;
    }

    public int getRowCount() {
        return rows.size();
    }

    public int getColumnCount() {
        return colNames.length;
    }

    public Object getValueAt(int r, int c) {
        Country country = rows.get(r);

        switch (c) {
            case 0:
                return country.getName();
            case 1:
                return country.getCapital();
            case 2:
                return country.getPopulation();
            case 3:
                return country.getFlag();
        }

        return null;
    }

    @Override
    public String getColumnName(int column) {
        return colNames[column];
    }

    @Override
    public Class<?> getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public boolean isCellEditable(int r, int c) {
        return c != 3;
    }

    @Override
    public void setValueAt(Object val, int r, int c) {
        Country country = rows.get(r);
        switch (c) {
            case 0:
                country.setName((String) val);
                break;
            case 1:
                country.setCapital((String) val);
                break;
            case 2:
                country.setPopulation((Double) val);
                break;
        }
    }
}
