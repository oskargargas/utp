package pl.edu.pjwstk.s8235.cw_2;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 06.10.2011
 * Time: 16:51
 */
public class Wyszukiwarka {

    public Wyszukiwarka() {

        JFileChooser fileChooser = new JFileChooser("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/UTP/");
		fileChooser.setFileFilter(new FileNameExtensionFilter(".java", "java"));
		fileChooser.setDialogType(JFileChooser.OPEN_DIALOG);
		fileChooser.setDialogTitle("Select java file.");

        if(fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {

            File f = fileChooser.getSelectedFile();
            int ifs = 0, words = 0;

            Pattern wordToLookFor = Pattern.compile("wariant");
            Pattern ifsPattern = Pattern.compile("\\s*if\\s*\\(.*?\\)");

            String fileContent = null;
            try {
                BufferedReader br = new BufferedReader(new FileReader(f));
                StringBuilder sb = new StringBuilder();

                String line;
                while((line = br.readLine()) != null) {
                    sb.append(line);
                    sb.append('\n');
                }

                br.close();

                fileContent = sb.toString();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Matcher m = wordToLookFor.matcher(fileContent);
            while(m.find())
                words++;

            fileContent = strip(fileContent);

            System.out.println(fileContent);

            m = ifsPattern.matcher(fileContent);
            while(m.find())
                ifs++;

            System.out.println("Liczba instrukcji if: " + ifs +
                    "\nLiczba napisów: " + words);

            try {

                File tmpDir = new File(System.getProperty("user.home") + "/tmp/");

                if(!tmpDir.exists())
                    if(!tmpDir.mkdir())
                        throw new IOException();

                FileWriter fw = new FileWriter(System.getProperty("user.home") + "/tmp/" + f.getName() + ".reg");
                fw.write("Liczba instrukcji if: " + ifs +
                        "\nLiczba napisów: " + words);
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String strip(String s) {
        StringBuilder sb = new StringBuilder(s);

        char before = 'a';
        int beforeIndex = 0;

        for (int i = 0; i < sb.length(); i++) {
            char c = sb.charAt(i);

            if (c == '"' && before != '\\' && before != '\'') {
                int futureIndex = i;
                do {
                    futureIndex = sb.indexOf("\"", futureIndex + 1);
                } while (sb.charAt(futureIndex - 1) == '\\');
                sb.delete(beforeIndex, futureIndex + 1);
                continue;
            }

            if (c == '/' && before == '/') {
                int futureIndex = sb.indexOf("\n", i + 1);
                sb.delete(beforeIndex, futureIndex);
                before = '\n';
                continue;
            }

            if (c == '*' && before == '/') {
                int futureIndex = sb.indexOf("*/", i + 1);
                sb.delete(beforeIndex, futureIndex + 2);
                continue;
            }

            before = c;
            beforeIndex = i;
        }

        return sb.toString();
    }

    public static void main(String[] args) {
        new Wyszukiwarka();
    }
}
