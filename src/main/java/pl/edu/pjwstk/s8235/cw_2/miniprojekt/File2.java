package pl.edu.pjwstk.s8235.cw_2.miniprojekt;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 11.10.2011
 * Time: 14:58
 */
public class File2 extends File {
    public File2(String filePath) {
        super(filePath);
    }

    public List<File> listFilesRecurse() {
        List<File> fileList = new ArrayList<File>();
        List<File2> dirList = new ArrayList<File2>();

        for (String s : list()) {
            File2 f = new File2(getPath() + "/" + s);
            if(f.isFile())
                fileList.add(f);
            else if (f.isDirectory())
                dirList.add(f);
        }

        for (File2 f : dirList)
            fileList.addAll(f.listFilesRecurse());

        return fileList;
    }

    public List<File> listFilesRecurse(FileFilter fileFilter) {
        List<File> fileList = listFilesRecurse();

        for (int i = 0; i < fileList.size(); i++)
            if(!fileFilter.accept(fileList.get(i)))
                fileList.remove(i);

        return fileList;
    }

    public List<File> listDirsRecurse() {
        List<File> dirList = new ArrayList<File>();
        List<File2> dir2List = new ArrayList<File2>();

        for (String s : list()) {
            File2 f = new File2(getPath() + "/" + s);
            if(f.isDirectory()) {
                dirList.add(f);
                dir2List.add(f);
            }
        }

        for (File2 f : dir2List)
             dirList.addAll(f.listDirsRecurse());

        return  dirList;
    }

    public List<File> listDirsRecurse(FileFilter fileFilter) {
        List<File> fileList = listDirsRecurse();

        for (int i = 0; i < fileList.size(); i++)
            if(!fileFilter.accept(fileList.get(i)))
                fileList.remove(i);

        return fileList;
    }

    public boolean deleteDir() {
        if(!isDirectory())
            return false;

        for (File f : listFilesRecurse())
            if(!f.delete())
                return false;

        for (String s : list()) {
            File2 f = new File2(getPath() + "/" + s);
            if (!f.deleteDir())
                return false;
        }

        return delete();
    }

    public List<String> readLines() {
        List<String> lines = new ArrayList<String>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(this));

            String line;
            while((line = br.readLine()) != null)
                lines.add(line);

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    public boolean copy(File toDir) {
        if(!isFile()) return false;

        File newFile = new File(toDir.getAbsolutePath() + "/" + getName());

        try {
            InputStream in = new FileInputStream(this);
            OutputStream out = new FileOutputStream(newFile);

            int len;
            byte[] buf = new byte[1024];
            while ((len = in.read(buf)) > 0)
                out.write(buf, 0, len);

            in.close();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean copy(String newName) {
        return copy(newName, false);
    }

    public boolean copy(String newName, boolean preserveTime) {
        if(!isFile()) return false;

        File newFile = new File(getPath() + "/" + newName);
        boolean retVal = renameTo(newFile);

        if(preserveTime && retVal)
            newFile.setLastModified(lastModified());

        return retVal;
    }

    public static void main(String[] args) {
        File2 f = new File2("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/ASD/Cwiczenia");
        List<File> l = f.listFilesRecurse();
        System.out.println(l);

        List<File> ld = f.listDirsRecurse();
        System.out.println(ld);

//        File2 fd = new File2("/Users/oskar/tmp/tmp-kopia");
//        System.out.println(fd.deleteDir());
    }

}
