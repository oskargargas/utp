package pl.edu.pjwstk.s8235.cw_4;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 20.10.2011
 * Time: 18:13
 */
public class AnagramList implements Comparable {
    private List<String> list;
    private int number;

    AnagramList(List<String> list) {
        this.list = list;
        this.number = list.size();
    }

    @Override
    public String toString() {
        return "" + number + " " + list.toString();
    }

    public int compareTo(Object o) {
        return ((AnagramList)o).getNumber() - number;
    }

    public int getNumber() {
        return number;
    }
}
