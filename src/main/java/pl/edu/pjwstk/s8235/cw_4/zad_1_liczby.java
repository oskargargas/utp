package pl.edu.pjwstk.s8235.cw_4;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 20.10.2011
 * Time: 16:57
 */

import javax.swing.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Zad. 1  (3 punkty)
 *
 * Stworzyć dwie listy (A i B) liczb (podawanych w dialogach jako liczby rozdzielone spacjami), a nastepnie:
 * a) stworzyc liste zawierajaca wszystkie liczby z obu list
 * b) stworzyc liste, ktora zawiara tylko te liczby z listy A, ktore wystepuja na liscie B
 * c) stworzyc liste, ktoa zawiera tylko te liczby z listy A, ktore nie wystepuja na liscie B.
 *
 * Przedstawic mozliwie najbardziej czytelny, prosty, a zarazem najkrotszy kod realizujacy te operacje
 *
 */

public class zad_1_liczby {
    public static void main(String[] args) {
        String[] listaA = JOptionPane.showInputDialog(null, "Podaj liczby dla listy A.").split("\\s+"),
                 listaB = JOptionPane.showInputDialog(null, "Podaj liczby dla listy B.").split("\\s+");

        Set<String> setA = new HashSet<String>(Arrays.asList(listaA));
        Set<String> setB = new HashSet<String>(Arrays.asList(listaB));

        Set<String> all = new HashSet<String>(setA);
        all.addAll(setB);

        Set<String> inter = new HashSet<String>(setA);
        inter.retainAll(setB);

        Set<String> diff = new HashSet<String>(setA);
        diff.removeAll(setB);

        System.out.println("A = " + setA);
        System.out.println("B = " + setB);

        System.out.println("Suma = " + all);
        System.out.println("Część wspólna = " + inter);
        System.out.println("A - B = " + diff);
    }
}
