package pl.edu.pjwstk.s8235.cw_4;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 20.10.2011
 * Time: 17:25
 */

import javax.swing.*;
import java.io.*;
import java.util.*;

/**
 * Zad. 2 (7 punktów)
 *
 * Na liście słów z http://www.puzzlers.org/pub/wordlists/unixdict.txt znaleźć wszystkie anagramy.
 * a) wypisać je dla wszystkich słów w porządku liczby  anagramów danego słowa
 * b) dla słowa podanego w dialogu wypisać anagramy w porządku alfabetycznym.
 *
 * Uwaga: lista słów jest tylko angielska.
 * anagramy = słowa składające się z tych samych znaków
 *
 */

public class zad_2_anagramy {
    public static void main(String[] args) {
        File f = new File("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/UTP/IDEA_UTP/src/main/java/pl/edu/pjwstk/s8235/cw_4/unixdict.txt");
        Map<String, List<String>> map = new HashMap<String, List<String>>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));

            String line;
            while((line = br.readLine()) != null) {

                String key = getKey(line);

                if (map.containsKey(key)) {
                    map.get(key).add(line);
                } else {
                    List<String> tmpList = new ArrayList<String>();
                    tmpList.add(line);
                    map.put(key, tmpList);
                }
            }

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (true) {
            PriorityQueue<AnagramList> pq = new PriorityQueue<AnagramList>();

            for (List<String> l : map.values())
                pq.add(new AnagramList(l));

            AnagramList al;
            while ((al = pq.poll()) != null)
                System.out.println(al);
        } else {
            String slowo = JOptionPane.showInputDialog(null, "Podaj słowo.").trim();
            String key = getKey(slowo);

            if (map.containsKey(key)) {
                System.out.println("Słowo '" + slowo + "' posiada następujące anagramy:");
                System.out.println(map.get(key));
            } else {
                System.out.println("Słowo '" + slowo + "' nie posiada anagramów.");
            }

        }

    }

    private static String getKey(String s) {
        char[] charTab = s.trim().toCharArray();
        Arrays.sort(charTab);
        return new String(charTab);
    }
}
