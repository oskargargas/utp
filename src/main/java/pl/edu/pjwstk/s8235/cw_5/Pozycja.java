package pl.edu.pjwstk.s8235.cw_5;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 27.10.2011
 * Time: 15:31
 */

public class Pozycja implements Comparable {

    private String identyfikator;
    private String nazwisko;
    private String towar;
    private double cena;
    private double ilosc;

    public Pozycja(String identyfikator, String nazwisko, String towar, double cena, double ilosc) {
        this.identyfikator = identyfikator;
        this.nazwisko = nazwisko;
        this.towar = towar;
        this.cena = cena;
        this.ilosc = ilosc;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(identyfikator).append(';');
        sb.append(nazwisko).append(';');
        sb.append(towar).append(';');
        sb.append(cena).append(';');
        sb.append(ilosc);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o == this) return true;
        if (o.getClass() != getClass())
            return false;

        Pozycja rhs = (Pozycja)o;
        return new EqualsBuilder()
                .append(identyfikator, rhs.identyfikator)
                .append(nazwisko, rhs.nazwisko)
                .append(towar, rhs.towar)
                .append(cena, rhs.cena)
                .append(ilosc, rhs.ilosc)
                .isEquals();
    }

    public int compareTo(Object o) {
        Pozycja rhs = (Pozycja)o;
        return (int)((rhs.cena * rhs.ilosc) - (cena * ilosc));
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(1373, 7187)
                .append(identyfikator)
                .append(nazwisko)
                .append(towar)
                .append(cena)
                .append(ilosc)
                .toHashCode();
    }

    public double getKoszt() {
        return cena * ilosc;
    }

    public String getIdentyfikator() {
        return identyfikator;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
