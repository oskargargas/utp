package pl.edu.pjwstk.s8235.cw_5;

import pl.edu.pjwstk.s8235.cw_2.miniprojekt.File2;

import java.text.Collator;
import java.util.Locale;
import java.util.TreeMap;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 27.10.2011
 * Time: 15:32
 */

public class Zad_1 {

    public static void main(String[] args) {
        File2 f = new File2("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/UTP/IDEA_UTP/src/main/java/pl/edu/pjwstk/s8235/cw_5/data_zad_1");
        TreeMap<String, Integer> map = new TreeMap<String, Integer>(Collator.getInstance(new Locale("pl")));

        for (String line : f.readLines()) {
            String[] ll = line.trim().split("[^\\wĄąĆćĘęŁłŃńÓóŚśŹźŻż]+");
            for (String s : ll) {
                if(map.get(s) == null)
                    map.put(s, 1);
                else
                    map.put(s, map.get(s) + 1);
            }
        }

        String key;
        while (!map.isEmpty()) {
            key = map.firstKey();
            System.out.println(key + " " + map.get(key));
            map.remove(key);
        }
    }
}
