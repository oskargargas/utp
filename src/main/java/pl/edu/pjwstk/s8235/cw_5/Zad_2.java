package pl.edu.pjwstk.s8235.cw_5;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import pl.edu.pjwstk.s8235.cw_2.miniprojekt.File2;

import java.text.Collator;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 27.10.2011
 * Time: 15:31
 */
public class Zad_2 {

    public static void main(String[] args) {
        File2 f = new File2("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/UTP/IDEA_UTP/src/main/java/pl/edu/pjwstk/s8235/cw_5/data_zad_2");

        Multimap<String, Pozycja> multimap = HashMultimap.create();


        for (String line : f.readLines()) {
            String[] t = line.trim().split(";");
            multimap.put(t[0], new Pozycja(t[0], t[1], t[2], Double.parseDouble(t[3]), Double.parseDouble(t[4])));
        }

        List<Pozycja> nazwiska = new ArrayList<Pozycja>(multimap.values());
        Collections.sort(nazwiska, new Comparator<Pozycja>() {
            public int compare(Pozycja a, Pozycja b) {
                int ret = Collator.getInstance(new Locale("pl")).compare(a.getNazwisko(), b.getNazwisko());

                if (ret == 0)
                    ret = a.getIdentyfikator().compareTo(b.getIdentyfikator());

                return ret;
            }
        });

        System.out.println("Nazwiska");

        for (Pozycja p : nazwiska)
            System.out.println(p);

        System.out.println();

        List<Pozycja> koszty = new ArrayList<Pozycja>(multimap.values());
        Collections.sort(koszty);

        System.out.println("Koszty");

        for (Pozycja p : koszty)
            System.out.println(p + " (koszt: " + p.getKoszt() + ")");

        System.out.println();

        System.out.println("Klient 1");
        for (Pozycja p : multimap.get("c00001"))
            System.out.println(p);

        System.out.println();

        System.out.println("Klient 2");
        for (Pozycja p : multimap.get("c00002"))
            System.out.println(p);
    }

}
