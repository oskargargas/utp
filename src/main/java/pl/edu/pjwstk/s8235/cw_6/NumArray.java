package pl.edu.pjwstk.s8235.cw_6;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 17.11.2011
 * Time: 16:37
 */
public class NumArray<N> {

    private Class retType;
    private Number[] numbers;
    private Map<Class, Method> operationsMap;
    private Map<Class, String> valueStringMap;

    public static NumArray of(Class t) {
        NumArray ret = new NumArray();
        ret.retType = t;

        return ret;
    }

    public static NumArray of(String className) {
        Class c;

        try {
            c = Class.forName(className);
        } catch (ClassNotFoundException e) {
            try {
                c = Class.forName("java.lang." + className);
            } catch (ClassNotFoundException e1) {
                throw new RuntimeException();
            }
        }

        return of(c);
    }

    public NumArray from(String s) {
        String[] numbersString = s.split("\\s+");
        numbers = new Number[numbersString.length];

        Method m = getOperationsMap().get(retType);

        for (int i = 0; i < numbersString.length; i++) {
            try {
                numbers[i] = (Number) m.invoke(retType, numbersString[i]);
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return this;
    }

    public NumArray from(File file) {
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));

            String line;
            while ((line = br.readLine()) != null)
                sb.append(line).append(" ");

            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return from(sb.toString());  //To change body of created methods use File | Settings | File Templates.
    }

    public Number sum() {
        Number sum = 0;

        for (Number n : numbers)
            sum = sum.doubleValue() + n.doubleValue();

        try {
            String retTypeValue = getValueStringMap().get(retType);
            Method m = sum.getClass().getMethod(retTypeValue);

            sum = (Number) retType.getConstructor(String.class).newInstance(m.invoke(sum).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sum;
    }

    private Map<Class, Method> getOperationsMap() {
        if (this.operationsMap != null)
            return this.operationsMap;

        Map<Class, Method> opMap = new HashMap<Class, Method>();
        try {
            opMap.put(Byte.class, Byte.class.getMethod("parseByte", String.class));
            opMap.put(Short.class, Short.class.getMethod("parseShort", String.class));
            opMap.put(Integer.class, Integer.class.getMethod("parseInt", String.class));
            opMap.put(Long.class, Long.class.getMethod("parseLong", String.class));
            opMap.put(Float.class, Float.class.getMethod("parseFloat", String.class));
            opMap.put(Double.class, Double.class.getMethod("parseDouble", String.class));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        this.operationsMap = opMap;
        return operationsMap;
    }

    private Map<Class, String> getValueStringMap() {
        if (this.valueStringMap != null)
            return this.valueStringMap;

        Map<Class, String> vsMap = new HashMap<Class, String>();
        vsMap.put(Byte.class, "byteValue");
        vsMap.put(Short.class, "shortValue");
        vsMap.put(Integer.class, "intValue");
        vsMap.put(Long.class, "longValue");
        vsMap.put(Float.class, "floatValue");
        vsMap.put(Double.class, "doubleValue");

        this.valueStringMap = vsMap;
        return valueStringMap;
    }
}
