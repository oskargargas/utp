package pl.edu.pjwstk.s8235.cw_6;

import javax.swing.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 02.11.2011
 * Time: 11:39
 */

public class Zad_2_kalkulator {

    private Map<String, Method> operationsMap;

    public Zad_2_kalkulator() {
        String[] operation;
        operation = JOptionPane.showInputDialog("Podaj liczby w formacie: A op B.").trim().split("[^0-9+-/*]+");
        System.out.println(calculate(operation[0], operation[2], operation[1]));
    }

    public static void main(String[] args) {
        new Zad_2_kalkulator();
    }

    private BigDecimal calculate(String sA, String sB, String op) {
        BigDecimal a = new BigDecimal(sA);
        BigDecimal b = new BigDecimal(sB);

        Map<String, Method> opMap = getOperationsMap();

        BigDecimal ret = null;

        try {
            ret = (BigDecimal) opMap.get(op).invoke(a, b);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return ret;
    }

    private Map<String, Method> getOperationsMap() {
        if (this.operationsMap != null)
            return this.operationsMap;

        Class<?> bd = BigDecimal.class;

        Map<String, Method> opMap = new HashMap<String, Method>();
        try {
            opMap.put("+", bd.getMethod("add", bd));
            opMap.put("-", bd.getMethod("subtract", bd));
            opMap.put("*", bd.getMethod("multiply", bd));
            opMap.put("/", bd.getMethod("divide", bd));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        this.operationsMap = opMap;
        return operationsMap;
    }
}
