package pl.edu.pjwstk.s8235.cw_6;

import java.io.File;

import static javax.swing.JOptionPane.showInputDialog;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 17.11.2011
 * Time: 16:36
 */
public class Zad_3 {

    public static void main(String[] args) throws Exception {

        Number sum = NumArray.of("Short").from("1 2 3").sum();
        System.out.println(sum);

        sum = NumArray.of("Integer").from("1 2 3").sum();
        System.out.println(sum);

        sum = NumArray.of("Float").from(new File("/Users/oskar/Dropbox/Studia/PJWSTK/Semestr 03/UTP/IDEA_UTP/src/main/java/pl/edu/pjwstk/s8235/cw_6/data_zad_3")).sum();
        System.out.println(sum);

        NumArray<Number> arr = NumArray.of(Double.class).from(showInputDialog("Podaj liczby"));
        System.out.println(arr.sum());
    }

}
