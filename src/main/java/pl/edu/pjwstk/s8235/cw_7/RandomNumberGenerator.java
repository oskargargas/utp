package pl.edu.pjwstk.s8235.cw_7;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Random;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 24.11.2011
 * Time: 19:39
 */
public class RandomNumberGenerator extends Thread {
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private Random generator;
    private int randomNumber;

    public RandomNumberGenerator() {
        randomNumber = 0;
        generator = new Random();
    }

    public int getRandomNumber() {
        return randomNumber;
    }

    synchronized public void setRandomNumber(int randomNumber) {
        int oldNumber = this.randomNumber;
        this.randomNumber = randomNumber;

        pcs.firePropertyChange("randomNumber", oldNumber, this.randomNumber);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void run() {
        while(true) {
            setRandomNumber((int)(generator.nextDouble() * 10) - 5);
            System.out.println("New random number is: " + getRandomNumber());
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
