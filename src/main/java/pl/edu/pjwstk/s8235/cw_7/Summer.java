package pl.edu.pjwstk.s8235.cw_7;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 25.11.2011
 * Time: 00:34
 */
public class Summer implements PropertyChangeListener {
    private int sum;

    public Summer() {
        this.sum = 0;
    }

    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
        if (propertyChangeEvent.getNewValue() instanceof Integer) {
            sum += (Integer) (propertyChangeEvent.getNewValue());
            System.out.println("New sum = " + sum);
        }
    }
}
