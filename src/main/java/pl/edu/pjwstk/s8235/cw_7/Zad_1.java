package pl.edu.pjwstk.s8235.cw_7;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 24.11.2011
 * Time: 19:37
 */
public class Zad_1 {

    public static void main(String[] args) {
        System.out.println("Hello world!");
        RandomNumberGenerator rng = new RandomNumberGenerator();
        Summer s = new Summer();
        rng.addPropertyChangeListener(s);
        rng.start();
    }

}
