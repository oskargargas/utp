package pl.edu.pjwstk.s8235.cw_8

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 24.11.2011
 * Time: 14:11
 */
class GroovyScripts {
    static def Map<String, BigDecimal> getRates() {
        def url = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml'
        def rates = new XmlParser().parse(url)
        def EURmap = [:]
        rates.Cube.Cube.Cube.each {
          EURmap[it.@currency] = it.@rate.toBigDecimal()
        }

        EURmap
    }

    static def translate(String s, String targetLanguage) {
        def text = URLEncoder.encode(s, "UTF-8")

        def detect = 'http://www.google.com/uds/GlangDetect?v=1.0&q=' + text
        def langDetectResponse = jsonToGroovyMap(detect.toURL().text)
        def lang = langDetectResponse.responseData.language

        def translate = "http://www.google.com/uds/Gtranslate?v=1.0&q=${text}&langpair=${lang}%7C${targetLanguage}"
        def translateResponse = jsonToGroovyMap(translate.toURL().text)

        translateResponse.responseStatus == 200 ? translateResponse.responseData.translatedText : translateResponse.responseDetails
    }

    /**
    * Converts a JSON string into a Groovy map.
    * The difference lies in the difference in map notation: {:} in JSON vs [:] in Groovy
    */
    private static def jsonToGroovyMap(jsonString) {
        new GroovyShell().evaluate(jsonString.replaceAll(/}/, ']').replaceAll(/\{/, '['))
    }
}
