package pl.edu.pjwstk.s8235.cw_8;

import java.math.BigDecimal;
import java.util.Map;

import static javax.swing.JOptionPane.showInputDialog;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 24.11.2011
 * Time: 14:10
 */
public class Zad_1 {
    public static void main(String[] args) {
        Map<String, BigDecimal> rates = GroovyScripts.getRates();

        double kwota = Double.parseDouble(showInputDialog("Podaj kwotę w EUR do przeliczenia.").trim());
        String waluta = showInputDialog("Podaj kod waluty").trim().toUpperCase();
        
        while (!rates.containsKey(waluta))
            waluta = showInputDialog("Podano błędny kod walut. Podaj kod waluty").trim().toUpperCase();

        System.out.println(kwota * rates.get(waluta).doubleValue());
    }
}
