package pl.edu.pjwstk.s8235.cw_8;

import static javax.swing.JOptionPane.showInputDialog;

/**
 * Created by IntelliJ IDEA.
 * User: Oskar Gargas
 * Date: 24.11.2011
 * Time: 18:51
 */
public class Zad_3 {
    public static void main(String[] args) {
        String text = showInputDialog("Podaj tekst do przetłumaczenia.").trim();
        String toLang = showInputDialog("Podaj język docelowy.").trim();

        System.out.println(GroovyScripts.translate(text, toLang));
    }
}
